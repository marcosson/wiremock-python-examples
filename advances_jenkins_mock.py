from wiremock.constants import Config
from wiremock.server.server import WireMockServer
from wiremock.client import Mapping, Mappings, MappingRequest, MappingResponse, HttpMethods

# Authentication service
authentication_mapping = Mapping(
    request=MappingRequest(method=HttpMethods.GET, url='/crumbIssuer/api/json'),
    response=MappingResponse(status=200,
                             headers={'Content-Type': 'application/json;charset=utf-8'},
                             body='{\"_class\":\"hudson.security.csrf.DefaultCrumbIssuer\",'
                                  '\"crumb\":\"74c509197a90fffef1485ee36e2945ea\",'
                                  '\"crumbRequestField\":\"Jenkins-Crumb\"}'
                             )
)

# Log service
log_mapping = Mapping(
    request=MappingRequest(method=HttpMethods.POST, url_pattern='/job/(.*?)/([0-9]*)/logText/progressiveText'),
    response=MappingResponse(status=200,
                             headers={'Content-Type': 'text/plain;charset=utf-8'},
                             body='Avviato dall\'utente Marco Passon\r\n'
                                  'Compilazione in corso nello spazio di lavoro '
                                  'C:\\Users\\Marco\\.jenkins\\workspace\\gmail-forTestOnCute\r\n'
                                  'gmail-forTestOnCute » default completed with result SUCCESS\r\n'
                                  'Finished: SUCCESS\r\n'
                             )
)

# Service that returns the configuration file of a job
config_mapping = Mapping(
    request=MappingRequest(method=HttpMethods.GET, url_pattern='/job/(.*?)/config.xml'),
    response=MappingResponse(status=200,
                             headers={'Content-Type': 'text/xml'},
                             body_file_name='body-for-cute-getAllProjectsConfiguration.xml'
                             )
)

# Service that enables a job
enable_mapping = Mapping(
    request=MappingRequest(method=HttpMethods.POST, url='/job/gmail-gonnaGetDeleted/enable'),
    response=MappingResponse(status=302, headers={'Location': 'http://localhost:8080/job/gmail-gonnaGetDeleted/'})
)

# Service that disables a job
disable_mapping = Mapping(
    request=MappingRequest(method=HttpMethods.POST, url='/job/gmail-gonnaGetDeleted/disable'),
    response=MappingResponse(status=302, headers={'Location': 'http://localhost:8080/job/gmail-gonnaGetDeleted/'})
)

# Service that checks if a job exists
exists_mapping = Mapping(
    request=MappingRequest(method=HttpMethods.HEAD, url='/job/gmail-forTestOnCute/api/json'),
    response=MappingResponse(status=200)
)

# Service that returns info about a (generic) job's build
build_mapping = Mapping(
    request=MappingRequest(method=HttpMethods.GET, url_pattern='/job/(.*?)/([0-9]*)/api/json'),
    response=MappingResponse(status=200,
                             headers={'Content-Type': 'application/json;charset=utf-8'},
                             body_file_name='body-for-cute-getBuildDetails.json'
                             )
)

# Queue service (response taken from Jenkins' documentation)
queue_mapping = Mapping(
    request=MappingRequest(method=HttpMethods.GET, url='/queue/api/json'),
    response=MappingResponse(status=200,
                             headers={'Content-Type': 'application/json;charset=utf-8'},
                             body_file_name='queue-list-example.json'
                             )
)

# Service that returns info about all the jobs in Jenkins
jobs_mapping = Mapping(
    request=MappingRequest(method=HttpMethods.GET, url='/api/json'),
    response=MappingResponse(status=200,
                             headers={'Content-Type': 'application/json;charset=utf-8'},
                             body_file_name='body-api-json-initial.json'
                             )
)

# Stateful behavior to create a new job
# Stub that handles the POST request sent when creating a new job from API
create_job_mapping = Mapping(
    scenario_name='Add job',
    required_scenario_state='Started',
    request=MappingRequest(method=HttpMethods.POST, url='/createItem?name=gmail-createdFromAPI'),
    response=MappingResponse(status=201),
    new_scenario_state='Job added'
)

# Stateful behavior to create a new job
# Stub that returns info about all the jobs after the insertion of a new job
created_job_mapping = Mapping(
    scenario_name='Add job',
    required_scenario_state='Job added',
    request=MappingRequest(method=HttpMethods.GET, url='/api/json'),
    response=MappingResponse(status=200,
                             headers={'Content-Type': 'application/json;charset=utf-8'},
                             body_file_name='body-api-json-insertion.json'
                             ),
    new_scenario_state='Started'
)

# Stateful behavior to delete a job
# Stub that handles the POST request sent when deleting a job from API
delete_job_mapping = Mapping(
    scenario_name='Delete job',
    required_scenario_state='Started',
    request=MappingRequest(method=HttpMethods.POST, url='/job/gmail-gonnaGetDeleted/doDelete'),
    response=MappingResponse(status=302),
    new_scenario_state='Job deleted'
)

# Stateful behavior to delete a job
# Stub that returns info about all the jobs after the deletion of a new job
deleted_job_mapping = Mapping(
    scenario_name='Delete job',
    required_scenario_state='Job deleted',
    request=MappingRequest(method=HttpMethods.GET, url='/api/json'),
    response=MappingResponse(status=200,
                             headers={'Content-Type': 'application/json;charset=utf-8'},
                             body_file_name='body-api-json-deleted.json'
                             ),
    new_scenario_state='Started'
)

# Stateful behavior to build a job
# Initial stub that returns info about a single job
job_mapping = Mapping(
    scenario_name='Build job',
    required_scenario_state='Started',
    request=MappingRequest(method=HttpMethods.GET, url_pattern='/job/(.*?)/api/json'),
    response=MappingResponse(status=200,
                             headers={'Content-Type': 'application/json;charset=utf-8'},
                             body_file_name='body-job-name-api-json-initial.json'
                             )
)

# Stateful behavior to build a job
# Stub that handles the POST request sent when building a job from API
build_job_mapping = Mapping(
    scenario_name='Build job',
    required_scenario_state='Started',
    request=MappingRequest(method=HttpMethods.POST, url_pattern='/job/(.*?)/build'),
    response=MappingResponse(status=201, headers={'Location': 'http://localhost:8080/queue/item/11/'}),
    new_scenario_state='Job built'
)

# Stateful behavior to build a job
# Stub that returns info about a single job after building it
built_job_mapping = Mapping(
    scenario_name='Build job',
    required_scenario_state='Job built',
    request=MappingRequest(method=HttpMethods.GET, url_pattern='/job/(.*?)/api/json'),
    response=MappingResponse(status=200,
                             headers={'Content-Type': 'application/json;charset=utf-8'},
                             body_file_name='body-job-name-api-json-built.json'
                             ),
    new_scenario_state='Started'
)

# Create a new WireMockServer instances passing the path of the java executable and
# the path of the wiremock-standalone jar and starts it
# Printing the message is needed to know at which port the mock is running (its decided
# by WireMock when the constructor gets called, it can't be defined by the user
wm = WireMockServer(java_path='C:/Program Files/Java/jre1.8.0_161/bin/java.exe',
                    jar_path='C:/Users/Marco/Documents/wiremock-standalone-2.14.0.jar')
Config.base_url = 'http://localhost:{}/__admin'.format(wm.port)
wm.start()
print('Mock started at port ' + str(wm.port))

# Load all the mappings into the running WireMock server
Mappings.create_mapping(mapping=authentication_mapping)
Mappings.create_mapping(mapping=log_mapping)
Mappings.create_mapping(mapping=config_mapping)
Mappings.create_mapping(mapping=enable_mapping)
Mappings.create_mapping(mapping=disable_mapping)
Mappings.create_mapping(mapping=exists_mapping)
Mappings.create_mapping(mapping=build_mapping)
Mappings.create_mapping(mapping=queue_mapping)
Mappings.create_mapping(mapping=jobs_mapping)
Mappings.create_mapping(mapping=create_job_mapping)
Mappings.create_mapping(mapping=created_job_mapping)
Mappings.create_mapping(mapping=delete_job_mapping)
Mappings.create_mapping(mapping=deleted_job_mapping)
Mappings.create_mapping(mapping=job_mapping)
Mappings.create_mapping(mapping=build_job_mapping)
Mappings.create_mapping(mapping=built_job_mapping)

# Input instruction that awaits until the user types 'Enter'
# Needed to let the mock run and be accessed from external sources
print('Press Enter to terminate mock...')
input()